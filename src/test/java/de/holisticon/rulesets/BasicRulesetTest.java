package de.holisticon.rulesets;

import com.google.common.collect.Sets;
import de.holisticon.actions.Action;
import de.holisticon.exceptions.UnsupportedActionCombinationException;
import de.holisticon.exceptions.UnsupportedActionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Set;

import static de.holisticon.actions.Action.*;
import static de.holisticon.actions.ActionResult.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class BasicRulesetTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private BasicRuleset ruleset;

    @Before
    public void init() {
        this.ruleset = RulesetFactory.BASIC;
    }

    @Test
    public void stoneRules() {
        assertEquals(ruleset.getResult(STONE, STONE), DRAW);
        assertEquals(ruleset.getResult(STONE, PAPER), LOSS);
        assertEquals(ruleset.getResult(STONE, SCISSOR), WIN);
    }

    @Test
    public void paperRules() {
        assertEquals(ruleset.getResult(PAPER, STONE), WIN);
        assertEquals(ruleset.getResult(PAPER, PAPER), DRAW);
        assertEquals(ruleset.getResult(PAPER, SCISSOR), LOSS);
    }

    @Test
    public void scissorRules() {
        assertEquals(ruleset.getResult(SCISSOR, STONE), LOSS);
        assertEquals(ruleset.getResult(SCISSOR, PAPER), WIN);
        assertEquals(ruleset.getResult(SCISSOR, SCISSOR), DRAW);
    }

    @Test
    public void isComplete() {
        Set<List<Action>> r = Sets.cartesianProduct(ruleset.getSupportedActions(), ruleset.getSupportedActions());
        try {
            r.forEach((l) -> ruleset.getResult(l.get(0), l.get(1)));
        } catch (UnsupportedActionCombinationException | UnsupportedActionException ex) {
            fail(ex.getMessage());
        }
        assertTrue(true);
    }

    @Test
    public void unsupportedAction() {
        thrown.expect(UnsupportedActionException.class);
        thrown.expectMessage(is(UnsupportedActionException.createMessage(WELL, RulesetFactory.BASIC)));
        ruleset.getResult(WELL, STONE);
    }

}
