package de.holisticon.rulesets;

import com.google.common.collect.Sets;
import de.holisticon.actions.Action;
import de.holisticon.exceptions.UnsupportedActionCombinationException;
import de.holisticon.exceptions.UnsupportedActionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Set;

import static de.holisticon.actions.Action.*;
import static de.holisticon.actions.ActionResult.*;
import static org.junit.Assert.*;

public class AdditionalRulesetTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private AdditionalRuleset ruleset;

    @Before
    public void init() {
        this.ruleset = RulesetFactory.ADDITIONAL;
    }

    @Test
    public void stoneRules() {
        assertEquals(ruleset.getResult(STONE, STONE), DRAW);
        assertEquals(ruleset.getResult(STONE, PAPER), LOSS);
        assertEquals(ruleset.getResult(STONE, SCISSOR), WIN);
        assertEquals(ruleset.getResult(STONE, WELL), LOSS);
    }

    @Test
    public void paperRules() {
        assertEquals(ruleset.getResult(PAPER, STONE), WIN);
        assertEquals(ruleset.getResult(PAPER, PAPER), DRAW);
        assertEquals(ruleset.getResult(PAPER, SCISSOR), LOSS);
        assertEquals(ruleset.getResult(PAPER, WELL), WIN);
    }

    @Test
    public void scissorRules() {
        assertEquals(ruleset.getResult(SCISSOR, STONE), LOSS);
        assertEquals(ruleset.getResult(SCISSOR, PAPER), WIN);
        assertEquals(ruleset.getResult(SCISSOR, SCISSOR), DRAW);
        assertEquals(ruleset.getResult(SCISSOR, WELL), LOSS);
    }

    @Test
    public void wellRules() {
        assertEquals(ruleset.getResult(WELL, STONE), WIN);
        assertEquals(ruleset.getResult(WELL, PAPER), LOSS);
        assertEquals(ruleset.getResult(WELL, SCISSOR), WIN);
        assertEquals(ruleset.getResult(WELL, WELL), DRAW);
    }

    @Test
    public void isComplete() {
        Set<List<Action>> r = Sets.cartesianProduct(ruleset.getSupportedActions(), ruleset.getSupportedActions());
        try {
            r.forEach((l) -> ruleset.getResult(l.get(0), l.get(1)));
        } catch (UnsupportedActionCombinationException | UnsupportedActionException ex) {
            fail(ex.getMessage());
        }
        assertTrue(true);
    }

}
