package de.holisticon.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.holisticon.actions.Action;
import de.holisticon.exceptions.UnsupportedActionException;
import de.holisticon.player.Computer;
import de.holisticon.rulesets.Ruleset;
import de.holisticon.rulesets.RulesetFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static de.holisticon.actions.Action.STONE;
import static de.holisticon.actions.Action.WELL;
import static de.holisticon.actions.ActionResult.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PveGameController.class)
@WebAppConfiguration
public class PveGameControllerTest {
    private MockMvc mockMvc;

    private ObjectMapper jacksonObjectMapper = new ObjectMapper();

    @Before
    public void init() {
        this.mockMvc = standaloneSetup(new PveGameController()).build();
    }

    @Test
    public void successfulPveGame() throws Exception {
        String name = "Max Mustermann";
        Action action = STONE;
        Ruleset ruleset = RulesetFactory.BASIC;
        PveGameSettings gs = new PveGameSettings(name, ruleset, action);
        String json = jacksonObjectMapper.writeValueAsString(gs);
        this.mockMvc.perform(post("/pve")
                .contentType("application/json")
                .content(json)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(jsonPath("$.player1.name", is(name)))
                .andExpect(jsonPath("$.player1.action", is(action.name())))
                .andExpect(jsonPath("$.player1.result", anyOf(is(WIN.name()), is(LOSS.name()), is(DRAW.name()))))
                .andExpect(jsonPath("$.player2.name", is(Computer.NAME)))
                .andExpect(jsonPath("$.player2.action", isA(String.class)))
                .andExpect(jsonPath("$.player2.result", anyOf(is(WIN.name()), is(LOSS.name()), is(DRAW.name()))));
    }

    @Test
    public void unsupportedAction() throws Exception {
        String name = "Max Mustermann";
        Action action = WELL;
        PveGameSettings gs = new PveGameSettings(name, RulesetFactory.BASIC, action);
        String json = jacksonObjectMapper.writeValueAsString(gs);
        this.mockMvc.perform(post("/pve")
                .contentType("application/json")
                .content(json)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(jsonPath("$.error", is(UnsupportedActionException.createMessage(action, RulesetFactory.BASIC))));
    }
}
