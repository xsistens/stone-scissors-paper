package de.holisticon.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.holisticon.actions.Action;
import de.holisticon.game.GameResult;
import de.holisticon.player.User;
import de.holisticon.rulesets.Ruleset;
import de.holisticon.rulesets.RulesetFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static de.holisticon.actions.Action.SCISSOR;
import static de.holisticon.actions.Action.STONE;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PvpGameController.class)
@WebAppConfiguration
public class PvpGameControllerTest {
    private MockMvc mockMvc;

    private ObjectMapper jacksonObjectMapper = new ObjectMapper();

    @Before
    public void init() {
        this.mockMvc = standaloneSetup(new PvpGameController()).build();
    }

    @Test
    public void successfulPvpGame() throws Exception {
        String player1 = "Max Mustermann";
        String player2 = "Erika Mustermann";
        Action player1Action = STONE;
        Action player2Action = SCISSOR;
        Ruleset ruleset = RulesetFactory.BASIC;

        //Initialize
        PvpGameSettings gs = new PvpGameSettings(player1, player2, ruleset);
        String json = jacksonObjectMapper.writeValueAsString(gs);
        this.mockMvc.perform(post("/pvp/game")
                .contentType("application/json")
                .content(json)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().string(is(PvpGameController.GAME_INITIALIZED)));

        //Player 1 Action
        this.mockMvc.perform(post("/pvp/player1/action")
                .contentType("application/json")
                .content("\"" + player1Action.name() + "\"")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().string(is(PvpGameController.actionSetMessage(player1Action))));

        //Player 2 Action
        this.mockMvc.perform(post("/pvp/player2/action")
                .contentType("application/json")
                .content("\"" + player2Action.name() + "\"")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().string(is(PvpGameController.actionSetMessage(player2Action))));

        //Player 1 Result
        String gr = jacksonObjectMapper.writeValueAsString(new GameResult(new User(player1), new User(player2), player1Action, player2Action, ruleset.getResult(player1Action, player2Action), ruleset.getResult(player2Action, player1Action)));

        this.mockMvc.perform(get("/pvp/player1/result")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().string(is(gr)));

        this.mockMvc.perform(get("/pvp/player2/result")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().string(is(gr)));
    }
}
