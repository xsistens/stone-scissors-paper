package de.holisticon.game;

import de.holisticon.exceptions.*;
import de.holisticon.player.User;
import de.holisticon.rulesets.RulesetFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static de.holisticon.actions.Action.*;
import static de.holisticon.actions.ActionResult.LOSS;
import static de.holisticon.actions.ActionResult.WIN;
import static de.holisticon.game.GameState.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class GameTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private User player1;
    private User player2;
    private PvpGame pvpGame;
    private PveGame pveGame;

    @Before
    public void init() {
        this.player1 = new User("Max Mustermann");
        this.player2 = new User("Erika Mustermann");
        this.pvpGame = new PvpGameImpl(player1, player2, RulesetFactory.BASIC);
        this.pveGame = new PveGameImpl(player1, RulesetFactory.ADDITIONAL);
    }

    @Test
    public void successfulPveGame() {
        assertThat(pveGame.getState(), is(STARTED));
        pveGame.setPlayer1Action(STONE);
        assertThat(pveGame.getState(), is(RESULT_AVAILABLE));
        pveGame.getPlayer1Result();
        assertThat(pveGame.getState(), is(STARTED));
    }

    @Test
    public void successfulPvpGame() {
        assertThat(pvpGame.getState(), is(STARTED));
        pvpGame.setPlayer1Action(STONE);
        assertThat(pvpGame.getState(), is(WAIT_FOR_PLAYER2_ACTION));
        pvpGame.setPlayer2Action(SCISSOR);
        assertThat(pvpGame.getState(), is(RESULT_AVAILABLE));
        assertEquals(pvpGame.getPlayer1Result(), new GameResult(player1, player2, STONE, SCISSOR, WIN, LOSS));
        assertThat(pvpGame.getState(), is(WAIT_FOR_PLAYER2_READ));
        assertEquals(pvpGame.getPlayer2Result(), new GameResult(player1, player2, STONE, SCISSOR, WIN, LOSS));
        assertThat(pvpGame.getState(), is(STARTED));
    }

    @Test
    public void waitForPlayer() {
        thrown.expect(WaitForPlayerException.class);
        pvpGame.setPlayer1Action(STONE);
        pvpGame.getPlayer1Result();
    }

    @Test
    public void setActionFirst() {
        thrown.expect(SetActionFirstException.class);
        pvpGame.getPlayer1Result();
    }

    @Test
    public void actionAlreadySet() {
        thrown.expect(ActionAlreadySetException.class);
        pvpGame.setPlayer1Action(STONE);
        pvpGame.setPlayer1Action(WELL);
    }

    @Test
    public void waitForRead() {
        thrown.expect(WaitForReadException.class);
        pvpGame.setPlayer1Action(STONE);
        pvpGame.setPlayer2Action(PAPER);
        pvpGame.getPlayer1Result();
        pvpGame.setPlayer1Action(STONE);
    }

    @Test
    public void unsupportedAction() {
        thrown.expect(UnsupportedActionException.class);
        pvpGame.setPlayer1Action(WELL);
    }
}
