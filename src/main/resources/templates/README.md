Stone Scissors Paper
============
A two player game where both players simultaniously perform an action and receive a result based on the combination of these acitons.
 The game is played by sending GET and POST request to the server (by default **localhost:8080**).
 The performed actions and needed setting are provided by an additional JSON payload. The results of the actions are also returned in JSON.

Rulesets
------------
Rulesets define the actions that are allowed to choose from and the respective results for the combination of actions. There are two different rulesets to chose from at the moment.

### Basic ###
This ruleset contains the actions {STONE, SCISSOR, PAPER} with the following rules:

 * STONE wins over SCISSOR
 * SCISSOR wins over PAPER
 * PAPER wins over STONE
 * everything else is a draw

### Additional ###
Adds a new action WELL to the Basic ruleset which comes with the following rules:

 * WELL wins over SCISSOR and STONE
 * WELL looses against PAPER

Game modes
------------
This game comes with two different game modes. One is a game against a computer and the other is played against another human player.

### PvE game ###
A game against the computer. This mode can be accessed by sending a POST request to the route **localhost:8080/pve**.
 Additionally the following json payload has to be submitted:

    {
        "name": "Max Mustermann",
        "action": "STONE",
        "ruleset": "BASIC"
    }

 * **name** is the name of the player.
 * **action** is the action the player chooses this round
 (possible values: "STONE", "SCISSOR", "PAPER", "WELL"[depending on the selected ruleset]).
 * **ruleset** is the ruleset for this round
 (possible values: "BASIC", "ADDITIONAL").

As an answer for this request you will receive the result of this round as JSON lookig similar to this:

    {
        "player1": {
            "name": "Max Mustermann",
            "action": "STONE",
            "result": "WIN"
        },
        "player2": {
            "name": "Computer",
            "action": "SCISSOR",
            "result": "LOSS"
        }
    }

If something unexpected happens a reasonable error message is provided via JSON.

### PvP game ###
A game against another human player. This mode can be accessed by sending a POST request to the route **localhost:8080/pvp/game**.
 Additionally the following json payload has to be submitted:

    {
        "player1": "Max Mustermann",
        "player2": "Erika Mustermann",
        "ruleset": "BASIC"
    }

  * **player1** is the name of the first player.
  * **player2** is the name of the second player.
  * **ruleset** is the ruleset for this game.

As an answer for this request you will receive the result of this round as JSON lookig similar to this:

    {
        "success": "Game successfully initialized."
    }

After that the players have to perform their actions by sending the following POST request to the route **localhost:8080/pvp/playerX/action** (where "X" is ether "1" or "2" depending on the player who should perform this action).
 Additionally the following json payload has to be submitted:


    {
        "action": "STONE"
    }

As an answer for this request you will receive the result of this round as JSON lookig similar to this:


    {
        "success": "Action set to STONE"
    }

If something unexpected happens or an unexpected action is performed a reasonable error message is provided via JSON.
 After the player has performed his action he can try to fetch the result with a simple GET request to the following route **localhost:8080/pvp/player1/result**.
 When the second player has also performed his action the player should receive a response similar to this:

    {
        "player1": {
            "name": "Mac Mustermann",
            "action": "STONE",
            "result": "LOSS"
        },
        "player2": {
            "name": "Erika Mustermann",
            "action": "PAPER",
            "result": "WIN"
        }
    }

After both players have read their result via the route **localhost:8080/pvp/playerX/result** (where "X" is ether "1" or "2" depending on the player who should receive the result) the players can start to perform their actions again and play another round.
