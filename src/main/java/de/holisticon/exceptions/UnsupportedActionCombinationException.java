package de.holisticon.exceptions;

import de.holisticon.actions.Action;
import de.holisticon.rulesets.Ruleset;

public class UnsupportedActionCombinationException extends GameException {

    public UnsupportedActionCombinationException(Action action1, Action action2, Ruleset ruleset) {
        super(getMessage(action1, action2, ruleset));
    }

    public static String getMessage(Action action1, Action action2, Ruleset ruleset) {
        return "The combination of the actions " + action1 + " and " + action2 + " is not allowed in this ruleset: " + ruleset;
    }
}
