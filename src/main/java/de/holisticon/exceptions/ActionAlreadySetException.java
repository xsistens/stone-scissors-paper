package de.holisticon.exceptions;

public class ActionAlreadySetException extends GameException {
    public static String message = "The action for this round is already set.";

    public ActionAlreadySetException() {
        super(message);
    }
}
