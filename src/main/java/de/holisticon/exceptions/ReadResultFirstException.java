package de.holisticon.exceptions;

public class ReadResultFirstException extends GameException {

    public static String message = "Read the result before you perform the next action.";

    public ReadResultFirstException() {
        super(message);
    }
}
