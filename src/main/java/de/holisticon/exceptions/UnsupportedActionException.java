package de.holisticon.exceptions;

import de.holisticon.actions.Action;
import de.holisticon.rulesets.Ruleset;

public class UnsupportedActionException extends GameException {
    public UnsupportedActionException(Action action, Ruleset ruleset) {
        super(createMessage(action, ruleset));
    }

    public static String createMessage(Action action, Ruleset ruleset) {
        return "The action " + action + " is not allowed in this ruleset " + ruleset;
    }
}
