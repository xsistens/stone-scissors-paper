package de.holisticon.exceptions;

public class WaitForReadException extends GameException {

    public static String message = "Wait until the other player read the result.";

    public WaitForReadException() {
        super(message);
    }
}
