package de.holisticon.exceptions;

public class WaitForPlayerException extends GameException {

    public static String message = "Wait for the other player to perform his action.";

    public WaitForPlayerException() {
        super(message);
    }
}
