package de.holisticon.exceptions;

public class SetActionFirstException extends GameException {

    public static String message = "Please set your action first.";

    public SetActionFirstException() {
        super(message);
    }
}
