package de.holisticon.web;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.holisticon.game.PvpGame;
import de.holisticon.rulesets.Ruleset;

/**
 * The settings for a pvp game {@link PvpGame}.
 */
@JsonDeserialize(using = PvpGameSettingsDeserializer.class)
public class PvpGameSettings {
    private String player1;
    private String player2;
    private Ruleset ruleset;

    public PvpGameSettings() {
    }

    public PvpGameSettings(String player1, String player2, Ruleset ruleset) {
        this.player1 = player1;
        this.player2 = player2;
        this.ruleset = ruleset;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public Ruleset getRuleset() {
        return ruleset;
    }

    public void setRuleset(Ruleset ruleset) {
        this.ruleset = ruleset;
    }
}
