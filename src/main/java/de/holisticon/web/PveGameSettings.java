package de.holisticon.web;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.holisticon.actions.Action;
import de.holisticon.game.PveGame;
import de.holisticon.rulesets.Ruleset;

/**
 * The settings for a pve game {@link PveGame}.
 */
@JsonDeserialize(using = PveGameSettingsDeserializer.class)
public class PveGameSettings {
    private String name;
    private Ruleset ruleset;
    private Action action;

    public PveGameSettings() {
    }

    public PveGameSettings(String name, Ruleset ruleset, Action action) {
        this.name = name;
        this.ruleset = ruleset;
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ruleset getRuleset() {
        return ruleset;
    }

    public void setRuleset(Ruleset ruleset) {
        this.ruleset = ruleset;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
