package de.holisticon.web;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.holisticon.actions.Action;
import de.holisticon.rulesets.Ruleset;

import java.io.IOException;

public class PveGameSettingsDeserializer extends JsonDeserializer<PveGameSettings> {

    private ObjectMapper jacksonObjectMapper = new ObjectMapper();

    @Override
    public PveGameSettings deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);

        JsonNode nameField = node.path("name");
        if (nameField.isMissingNode() || !nameField.isTextual()) {
            throw new JsonParseException("Missing field 'name' with type 'String'.", jp.getCurrentLocation());
        }
        String name = nameField.asText();

        JsonNode rulesetField = node.path("ruleset");
        if (rulesetField.isMissingNode()) {
            throw new JsonParseException("Missing field 'ruleset' with type 'String'.", jp.getCurrentLocation());
        }
        Ruleset ruleset = jacksonObjectMapper.readerFor(Ruleset.class).readValue(rulesetField);

        JsonNode actionField = node.path("action");
        if (actionField.isMissingNode() || !actionField.isTextual()) {
            throw new JsonParseException("Missing field 'action' with type 'String'.", jp.getCurrentLocation());
        }
        String actionName = actionField.asText();

        Action action;
        try {
            action = Action.valueOf(actionName);
        } catch (IllegalArgumentException ex) {
            throw new JsonParseException(actionName + " is no valid action. Please use one of the following actions: " + ruleset.getSupportedActions().toString(), jp.getCurrentLocation());
        }
        return new PveGameSettings(name, ruleset, action);
    }
}