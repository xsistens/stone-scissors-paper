package de.holisticon.web;

import de.holisticon.actions.Action;
import de.holisticon.game.GameResult;
import de.holisticon.game.PvpGame;
import de.holisticon.game.PvpGameImpl;
import de.holisticon.player.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/pvp")
@RestController
public class PvpGameController extends DefaultExceptionHandler {

    public static final String GAME_INITIALIZED = "{\"success\": \"Game successfully initialized.\"}";
    public static final String INITIALIZE_FIRST = "{\"error\": \"Initialize game first with \"/pvp/game\"\"}";
    private User pvpPlayer1 = null;
    private User pvpPlayer2 = null;
    private PvpGame pvpGame = null;

    public static String actionSetMessage(Action action) {
        return "{\"success\": \"Action set to " + action + "\"}";
    }

    /**
     * Prepares a game against another player.
     *
     * @param settings contains the usernames for player1 and player2 and the ruleset for this game.
     * @return String returns a confirmation or an error in json form.
     */
    @RequestMapping(value = "/game", method = RequestMethod.POST)
    public String pvpGamePlayer1(@RequestBody PvpGameSettings settings) {
        if (pvpPlayer1 == null)
            pvpPlayer1 = new User(settings.getPlayer1());
        else
            pvpPlayer1.setName(settings.getPlayer1());

        if (pvpPlayer2 == null)
            pvpPlayer2 = new User(settings.getPlayer2());
        else
            pvpPlayer2.setName(settings.getPlayer2());

        if (pvpGame == null)
            pvpGame = new PvpGameImpl(pvpPlayer1, pvpPlayer2, settings.getRuleset());
        else
            pvpGame.setRuleset(settings.getRuleset());
        return GAME_INITIALIZED;
    }

    /**
     * Performs the action for player1.
     *
     * @param action the action for player1 in this round.
     * @return String returns a confirmation or an error in json form.
     */
    @RequestMapping(value = "/player1/action", method = RequestMethod.POST)
    public String pvpGamePlayer1Action(@RequestBody Action action) {
        if (pvpGame != null) {
            pvpGame.setPlayer1Action(action);
            return actionSetMessage(action);
        } else
            return INITIALIZE_FIRST;
    }

    /**
     * Performs the action for player2.
     *
     * @param action the action for player2 in this round.
     * @return String returns a confirmation or an error in json form.
     */
    @RequestMapping(value = "/player2/action", method = RequestMethod.POST)
    public String pvpGamePlayer2Action(@RequestBody Action action) {
        if (pvpGame != null) {
            pvpGame.setPlayer2Action(action);
            return actionSetMessage(action);
        } else
            return INITIALIZE_FIRST;
    }

    /**
     * Returns the result of this round for player1.
     *
     * @return {@link GameResult}.
     */
    @RequestMapping(value = "/player1/result", method = RequestMethod.GET)
    public GameResult pvpGamePlayer1Result() {
        if (pvpGame != null) {
            return pvpGame.getPlayer1Result();
        } else
            throw new RuntimeException(INITIALIZE_FIRST);
    }

    /**
     * Returns the result of this round for player2.
     *
     * @return {@link GameResult}.
     */
    @RequestMapping(value = "/player2/result", method = RequestMethod.GET)
    public GameResult pvpGamePlayer2Result() {
        if (pvpGame != null) {
            return pvpGame.getPlayer2Result();
        } else
            throw new RuntimeException(INITIALIZE_FIRST);
    }

}
