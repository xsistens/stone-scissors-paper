package de.holisticon.web;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.holisticon.rulesets.Ruleset;

import java.io.IOException;

public class PvpGameSettingsDeserializer extends JsonDeserializer<PvpGameSettings> {

    private ObjectMapper jacksonObjectMapper = new ObjectMapper();

    @Override
    public PvpGameSettings deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);

        JsonNode player1Field = node.path("player1");
        if (player1Field.isMissingNode() || !player1Field.isTextual()) {
            throw new JsonParseException("Missing field 'player1' with type 'String'.", jp.getCurrentLocation());
        }
        String player1 = player1Field.asText();

        JsonNode player2Field = node.path("player2");
        if (player2Field.isMissingNode() || !player2Field.isTextual()) {
            throw new JsonParseException("Missing field 'player2' with type 'String'.", jp.getCurrentLocation());
        }
        String player2 = player2Field.asText();

        JsonNode rulesetField = node.path("ruleset");
        if (rulesetField.isMissingNode()) {
            throw new JsonParseException("Missing field 'ruleset' with type 'String'.", jp.getCurrentLocation());
        }
        Ruleset ruleset = jacksonObjectMapper.readerFor(Ruleset.class).readValue(rulesetField);

        return new PvpGameSettings(player1, player2, ruleset);
    }
}