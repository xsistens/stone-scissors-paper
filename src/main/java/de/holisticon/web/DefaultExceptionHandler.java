package de.holisticon.web;

import com.fasterxml.jackson.core.JsonParseException;
import de.holisticon.exceptions.GameException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class DefaultExceptionHandler {
    /**
     * Handle the game exceptions and returns an appropriate json response.
     */
    @ExceptionHandler(GameException.class)
    public String handleCustomException(GameException ex) {
        return "{\"error\": \"" + ex.getMessage() + "\"}";
    }

    /**
     * Handle the exceptions thrown during the parsing phase for the json payloads of the routes.
     * Returns an appropriate json response.
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public String handleCustomException(HttpMessageNotReadableException ex) {
        String error;
        if (ex.getCause() instanceof JsonParseException) {
            error = ((JsonParseException) ex.getCause()).getOriginalMessage();
        } else {
            error = ex.getMessage();
        }
        return "{\"parsingError\": \"" + error + "\"}";
    }
}
