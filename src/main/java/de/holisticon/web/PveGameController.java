package de.holisticon.web;

import de.holisticon.game.GameResult;
import de.holisticon.game.PveGame;
import de.holisticon.game.PveGameImpl;
import de.holisticon.player.User;
import de.holisticon.rulesets.Ruleset;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/pve")
@RestController
public class PveGameController extends DefaultExceptionHandler {

    private User pvePlayer = null;
    private PveGame pveGame = null;

    /**
     * Starts a game against the computer.
     *
     * @param settings contains the username of the player, the ruleset for this game and
     *                 the chosen action for this round.
     * @return {@link GameResult} returns the result of this round as json.
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public GameResult pveGame(@RequestBody PveGameSettings settings) {
        Ruleset ruleset = settings.getRuleset();
        if (pvePlayer == null) {
            pvePlayer = new User(settings.getName());
        } else {
            pvePlayer.setName(settings.getName());
        }
        if (pveGame == null) {
            pveGame = new PveGameImpl(pvePlayer, ruleset);
            pveGame.setPlayer1Action(settings.getAction());
        } else {
            pveGame.setRuleset(ruleset);
            pveGame.setPlayer1Action(settings.getAction());
        }
        return pveGame.getPlayer1Result();
    }
}
