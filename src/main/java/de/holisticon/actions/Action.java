package de.holisticon.actions;

/**
 * Possible Actions for the game.
 */
public enum Action {
    STONE, PAPER, SCISSOR, WELL
}
