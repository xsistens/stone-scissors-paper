package de.holisticon.actions;

/**
 * Possible Results for the game.
 */
public enum ActionResult {
    /**
     * The player has won the game.
     */
    WIN,

    /**
     * The player has lost the game.
     */
    LOSS,

    /**
     * The player got a draw.
     */
    DRAW,
}
