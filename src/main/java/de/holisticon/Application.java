package de.holisticon;

import com.github.rjeschke.txtmark.Processor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

@SpringBootApplication
@RestController
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Shows a README for the game with all the possible game modes, actions and necessary JSON payloads.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        String result;
        try {
            InputStream markdownStream = new ClassPathResource("templates/README.md").getInputStream();
            result = Processor.process(markdownStream);
        } catch (IOException ex) {
            result = "No README.md found!";
        }
        StringWriter writer = new StringWriter();
        writer.append("<!DOCTYPE html><html><head><title>Home</title><meta charset=\"utf-8\"></head><body>");
        writer.append(result);
        writer.append("</body></html>");
        return writer.toString();
    }
}
