package de.holisticon.rulesets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.holisticon.actions.Action;
import de.holisticon.actions.ActionResult;
import de.holisticon.exceptions.UnsupportedActionCombinationException;
import de.holisticon.exceptions.UnsupportedActionException;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Basic functionalities based on the cartesian product of a set of supported actions and their appropriate results.
 */
abstract class AbstractRuleset implements Ruleset {

    private Map<Pair<Action, Action>, ActionResult> ruleMap = new HashMap<>();
    private HashSet<Action> supportedActions = new HashSet<>();

    AbstractRuleset() {
    }

    /**
     * Adds a new rule to this ruleset based on an action pair and the expected result.
     * The result is from the view of the provider of the left action.
     *
     * @param pair   the pair of actions that lead to the result.
     * @param result the appropriate result for the action pair from the view of the left action of the pair.
     */
    void addRule(Pair<Action, Action> pair, ActionResult result) {
        supportedActions.add(pair.getLeft());
        supportedActions.add(pair.getRight());
        ruleMap.put(pair, result);
    }

    public abstract String getName();

    @Override
    @JsonIgnore
    public Set<Action> getSupportedActions() {
        return supportedActions;
    }

    @Override
    public ActionResult getResult(Action action1, Action action2) throws UnsupportedActionException, UnsupportedActionCombinationException {
        if (!supportedActions.contains(action1))
            throw new UnsupportedActionException(action1, this);
        if (!supportedActions.contains(action2))
            throw new UnsupportedActionException(action2, this);
        ActionResult result = ruleMap.get(Pair.of(action1, action2));
        if (result == null)
            throw new UnsupportedActionCombinationException(action1, action2, this);
        return result;
    }

    @Override
    public String toString() {
        return getName();
    }
}
