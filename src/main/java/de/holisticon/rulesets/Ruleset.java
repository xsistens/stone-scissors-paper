package de.holisticon.rulesets;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.holisticon.actions.Action;
import de.holisticon.actions.ActionResult;
import de.holisticon.game.Game;

import java.util.Set;

/**
 * The ruleset for a game {@link Game}.
 */
@JsonDeserialize(using = RulesetDeserializer.class)
@JsonSerialize(using = RulesetSerializer.class)
public interface Ruleset {
    /**
     * Returns the name of ruleset.
     *
     * @return the name
     */
    String getName();

    /**
     * Returns a set of all the actions {@link Action} that are allowed in this ruleset {@link Ruleset}.
     *
     * @return {@link Set<Action>} all possible actions.
     */
    Set<Action> getSupportedActions();

    /**
     * Returns the result {@link ActionResult} for two actions {@link Action} from the view of the action1.
     *
     * @param action1 the action for which the result is returned.
     * @param action2 the action where action1 is compared to.
     * @return the result {@link ActionResult} from the view of the action1.
     */
    ActionResult getResult(Action action1, Action action2);
}
