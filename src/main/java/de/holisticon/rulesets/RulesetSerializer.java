package de.holisticon.rulesets;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

class RulesetSerializer extends JsonSerializer<Ruleset> {
    @Override
    public void serialize(Ruleset value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeString(value.getName());
    }
}
