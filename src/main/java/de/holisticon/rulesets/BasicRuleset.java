package de.holisticon.rulesets;

import de.holisticon.actions.Action;
import org.apache.commons.lang3.tuple.Pair;

import static de.holisticon.actions.Action.*;
import static de.holisticon.actions.ActionResult.*;

/**
 * The standard ruleset for the game stone-scissors-paper based on the actions {STONE, SCISSOR, PAPER}.
 */
class BasicRuleset extends AbstractRuleset {

    static final String NAME = "BASIC";

    private static BasicRuleset instance = null;

    protected BasicRuleset() {
        super();
        addRule(Pair.of(Action.STONE, STONE), DRAW);
        addRule(Pair.of(Action.STONE, PAPER), LOSS);
        addRule(Pair.of(Action.STONE, SCISSOR), WIN);

        addRule(Pair.of(Action.PAPER, PAPER), DRAW);
        addRule(Pair.of(Action.PAPER, SCISSOR), LOSS);
        addRule(Pair.of(Action.PAPER, STONE), WIN);

        addRule(Pair.of(Action.SCISSOR, SCISSOR), DRAW);
        addRule(Pair.of(Action.SCISSOR, STONE), LOSS);
        addRule(Pair.of(Action.SCISSOR, PAPER), WIN);
    }

    static BasicRuleset getInstance() {
        if (instance == null) {
            instance = new BasicRuleset();
        }
        return instance;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
