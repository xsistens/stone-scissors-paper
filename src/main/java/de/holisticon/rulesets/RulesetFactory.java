package de.holisticon.rulesets;

public class RulesetFactory {
    public static BasicRuleset BASIC = BasicRuleset.getInstance();
    public static AdditionalRuleset ADDITIONAL = AdditionalRuleset.getInstance();
}
