package de.holisticon.rulesets;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class RulesetDeserializer extends JsonDeserializer<Ruleset> {

    @Override
    public Ruleset deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);
        if (!node.isTextual()) {
            throw new JsonParseException("Field must be of type 'String'.", jp.getCurrentLocation());
        }
        String rulesetName = node.asText();
        Ruleset ruleset;
        if (StringUtils.equals(rulesetName, BasicRuleset.NAME)) {
            ruleset = RulesetFactory.BASIC;
        } else if (StringUtils.equals(rulesetName, AdditionalRuleset.NAME)) {
            ruleset = RulesetFactory.ADDITIONAL;
        } else {
            throw new JsonParseException(rulesetName + " is not a valid ruleset. Please choose one of following rulesets: \"" + BasicRuleset.NAME + "\" or \"" + AdditionalRuleset.NAME + "\".", jp.getCurrentLocation());
        }
        return ruleset;
    }
}