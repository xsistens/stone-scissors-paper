package de.holisticon.rulesets;

import org.apache.commons.lang3.tuple.Pair;

import static de.holisticon.actions.Action.*;
import static de.holisticon.actions.ActionResult.*;

/**
 * The extended ruleset for the game stone-scissors-paper based on the basic ruleset with an additional action WELL.
 */
class AdditionalRuleset extends BasicRuleset {

    static final String NAME = "ADDITIONAL";

    private static AdditionalRuleset instance = null;

    protected AdditionalRuleset() {
        super();
        addRule(Pair.of(WELL, STONE), WIN);
        addRule(Pair.of(WELL, PAPER), LOSS);
        addRule(Pair.of(WELL, SCISSOR), WIN);
        addRule(Pair.of(WELL, WELL), DRAW);

        addRule(Pair.of(STONE, WELL), LOSS);
        addRule(Pair.of(PAPER, WELL), WIN);
        addRule(Pair.of(SCISSOR, WELL), LOSS);
    }

    public static AdditionalRuleset getInstance() {
        if (instance == null) {
            instance = new AdditionalRuleset();
        }
        return instance;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
