package de.holisticon.player;

import de.holisticon.actions.Action;

import java.security.SecureRandom;
import java.util.List;

/**
 * A Computer player for the game, performing random actions.
 */
public class Computer implements Player {

    public static final String NAME = "Computer";

    private SecureRandom random = new SecureRandom();

    public Computer() {
    }

    /**
     * Returns a random action {@link Action} from the given set of possible actions.
     *
     * @param actions the possible actions from which the computer can choose from.
     */
    public Action getAction(List<Action> actions) {
        return actions.get(random.nextInt(actions.size()));
    }

    @Override
    public String getName() {
        return NAME;
    }
}
