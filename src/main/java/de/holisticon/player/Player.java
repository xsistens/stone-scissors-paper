package de.holisticon.player;

import de.holisticon.game.Game;

/**
 * A Player participation in the game {@link Game}.
 */
public interface Player {

    /**
     * Returns the name of the player.
     *
     * @return String
     */
    String getName();
}
