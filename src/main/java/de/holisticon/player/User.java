package de.holisticon.player;

/**
 * A Human player for the game.
 */
public class User implements Player {

    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the player.
     */
    public void setName(String name) {
        this.name = name;
    }
}
