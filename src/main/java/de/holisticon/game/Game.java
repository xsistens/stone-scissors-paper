package de.holisticon.game;

import de.holisticon.actions.Action;
import de.holisticon.exceptions.SetActionFirstException;
import de.holisticon.exceptions.WaitForPlayerException;
import de.holisticon.player.Player;
import de.holisticon.rulesets.Ruleset;

/**
 * Interface for a game of stone-scissors-paper.
 */
public interface Game {
    /**
     * Returns player1 of the game
     *
     * @return {@link Player}
     */
    Player getPlayer1();

    /**
     * Returns player2 of the game
     *
     * @return {@link Player}
     */
    Player getPlayer2();

    /**
     * Returns the current ruleset of the game.
     *
     * @return {@link Ruleset}
     */
    Ruleset getRuleset();

    /**
     * Changes the current ruleset of the game. The ruleset only will change if the game is in the state STARTED.
     *
     * @return boolean true if the ruleset was changed, false if not.
     */
    boolean setRuleset(Ruleset ruleset);

    /**
     * Returns the action of the player1 for the current round of the game.
     *
     * @return {@link Action} can be null, if the player has not performed his action yet.
     */
    Action getPlayer1Action();

    /**
     * Returns the action of the player2 for the current round of the game.
     *
     * @return {@link Action} can be null, if the player has not performed his action yet.
     */
    Action getPlayer2Action();

    /**
     * Returns the result of the game if it's already available. Result can be fetched until both players have received
     * their Result with the respective getPlayer1Result() or getResultPlayerw() method.
     *
     * @return {@link GameResult}
     * @throws SetActionFirstException if you try to get the result before performing an action.
     * @throws WaitForPlayerException  if the result is not present yet, because the other player has to perform his
     *                                 action first.
     */
    GameResult getPlayer1Result() throws SetActionFirstException, WaitForPlayerException;

    /**
     * Returns the result of the game if it's already available. Result can be fetched until both players have received
     * their Result with the respective getPlayer1Result() or getResultPlayerw() method.
     *
     * @return {@link GameResult}
     * @throws SetActionFirstException if you try to get the result before performing an action.
     * @throws WaitForPlayerException  if the result is not present yet, because the other player has to perform his
     *                                 action first.
     */
    GameResult getPlayer2Result() throws SetActionFirstException, WaitForPlayerException;

    /**
     * Returns the result of the game if it's already available. Result can be fetched until both players have received
     * their Result with the respective getPlayer1Result() or getResultPlayerw() method.
     *
     * @return {@link GameState}
     */
    GameState getState();
}
