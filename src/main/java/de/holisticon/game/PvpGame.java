package de.holisticon.game;

import de.holisticon.actions.Action;
import de.holisticon.exceptions.ActionAlreadySetException;
import de.holisticon.exceptions.ReadResultFirstException;
import de.holisticon.exceptions.UnsupportedActionException;
import de.holisticon.exceptions.WaitForReadException;
import de.holisticon.rulesets.Ruleset;

public interface PvpGame extends Game {
    /**
     * Sets the action {@link Action} for player2 {@link Game#getPlayer2()} in the current round.
     *
     * @throws UnsupportedActionException if the provided action is not part of the current ruleset {@link Ruleset}.
     * @throws ActionAlreadySetException  if the player has already set an action for this round.
     * @throws WaitForReadException       if the player has already set an action for this round.
     * @throws ReadResultFirstException   if the result for this round was not read {@link Game#getPlayer2Result()} from the player2 yet.
     */
    void setPlayer1Action(Action action) throws UnsupportedActionException, ActionAlreadySetException, WaitForReadException, ReadResultFirstException;

    /**
     * Sets the action {@link Action} for player2 {@link Game#getPlayer2()} in the current round.
     *
     * @throws UnsupportedActionException if the provided action is not part of the current ruleset {@link Ruleset}.
     * @throws ActionAlreadySetException  if the player has already set an action for this round.
     * @throws WaitForReadException       if the player has already set an action for this round.
     * @throws ReadResultFirstException   if the result for this round was not read {@link Game#getPlayer2Result()} from the player2 yet.
     */
    void setPlayer2Action(Action action) throws UnsupportedActionException, ActionAlreadySetException, WaitForReadException, ReadResultFirstException;
}
