package de.holisticon.game;

import de.holisticon.actions.Action;
import de.holisticon.exceptions.*;
import de.holisticon.player.Player;
import de.holisticon.rulesets.Ruleset;

import static de.holisticon.game.GameState.*;

/**
 * Absract class for the game of stone-scissors-paper. Holds a ruleset {@link Ruleset} to identify which player wins
 * and controls the state flow of the game.
 */
public abstract class AbstractGame implements Game {

    public Ruleset ruleset;
    private GameState state = STARTED;
    private GameResult result = null;
    private Action player1Action = null;
    private Action player2Action = null;

    public AbstractGame(Ruleset ruleset) {
        this.ruleset = ruleset;
    }

    /**
     * Calculates the current result {@link GameResult} based on the given ruleset {@link Ruleset} and safes it to
     * make it readable for the players.
     */
    private void calculateResults() throws UnsupportedActionException, UnsupportedActionCombinationException {
        result = new GameResult(getPlayer1(), getPlayer2(), player1Action, player2Action, ruleset.getResult(player1Action, player2Action), ruleset.getResult(player2Action, player1Action));
        switchState(RESULT_AVAILABLE);
    }

    /**
     * Switches the state {@link GameState} of the game and performs additional actions based on the new state.
     *
     * @param state the next state for the game.
     */
    private void switchState(GameState state) {
        this.state = state;
        switch (state) {
            case STARTED:
                player1Action = null;
                player2Action = null;
                result = null;
                break;
            default:
                break;
        }
    }

    public Action getPlayer1Action() {
        return player1Action;
    }

    /**
     * Sets the action {@link Action} for player1 {@link Game#getPlayer1()} in the current round.
     *
     * @param action the action which should be performed.
     * @throws UnsupportedActionException if the provided action is not part of the current ruleset {@link Ruleset}.
     * @throws ActionAlreadySetException  if the player has already set an action for this round.
     * @throws WaitForReadException       if the player has already set an action for this round.
     * @throws ReadResultFirstException   if the result for this round was not read
     *                                    {@link AbstractGame#getPlayer1Result()} from the player1 yet.
     */
    public void setPlayer1Action(Action action) throws UnsupportedActionException, ActionAlreadySetException, WaitForReadException, ReadResultFirstException {
        switch (state) {
            case WAIT_FOR_PLAYER2_READ:
                throw new WaitForReadException();
            case WAIT_FOR_PLAYER2_ACTION:
                throw new ActionAlreadySetException();
            case WAIT_FOR_PLAYER1_ACTION:
                if (!ruleset.getSupportedActions().contains(action))
                    throw new UnsupportedActionException(action, ruleset);
                player1Action = action;
                calculateResults();
                break;
            case STARTED:
                if (!ruleset.getSupportedActions().contains(action))
                    throw new UnsupportedActionException(action, ruleset);
                player1Action = action;
                switchState(WAIT_FOR_PLAYER2_ACTION);
                break;
            default:
                throw new ReadResultFirstException();
        }
    }

    public Action getPlayer2Action() {
        return player2Action;
    }

    /**
     * Sets the action {@link Action} for player2 {@link Game#getPlayer2()} in the current round.
     *
     * @param action the action which should be performed.
     * @throws UnsupportedActionException if the provided action is not part of the current ruleset {@link Ruleset}.
     * @throws ActionAlreadySetException  if the player has already set an action for this round.
     * @throws WaitForReadException       if the player has already set an action for this round.
     * @throws ReadResultFirstException   if the result for this round was not read {@link Game#getPlayer2Result()}
     *                                    from the player2 yet.
     */
    public void setPlayer2Action(Action action) throws UnsupportedActionException, ActionAlreadySetException, WaitForReadException, ReadResultFirstException {
        switch (state) {
            case WAIT_FOR_PLAYER1_READ:
                throw new WaitForReadException();
            case WAIT_FOR_PLAYER1_ACTION:
                throw new ActionAlreadySetException();
            case WAIT_FOR_PLAYER2_ACTION:
                if (!ruleset.getSupportedActions().contains(action))
                    throw new UnsupportedActionException(action, ruleset);
                player2Action = action;
                calculateResults();
                break;
            case STARTED:
                if (!ruleset.getSupportedActions().contains(action))
                    throw new UnsupportedActionException(action, ruleset);
                player2Action = action;
                switchState(WAIT_FOR_PLAYER1_ACTION);
                break;
            default:
                throw new ReadResultFirstException();
        }
    }

    public GameResult getPlayer1Result() throws SetActionFirstException, WaitForPlayerException {
        GameResult lastResult;
        switch (state) {
            case WAIT_FOR_PLAYER1_READ:
                lastResult = result;
                switchState(STARTED);
                return lastResult;
            case RESULT_AVAILABLE:
                lastResult = result;
                switchState(WAIT_FOR_PLAYER2_READ);
                return lastResult;
            case WAIT_FOR_PLAYER2_READ:
                return result;
            case WAIT_FOR_PLAYER2_ACTION:
                throw new WaitForPlayerException();
            default:
                throw new SetActionFirstException();
        }
    }

    public GameResult getPlayer2Result() throws SetActionFirstException, WaitForPlayerException {
        GameResult lastResult;
        switch (state) {
            case WAIT_FOR_PLAYER2_READ:
                lastResult = result;
                switchState(STARTED);
                return lastResult;
            case RESULT_AVAILABLE:
                lastResult = result;
                switchState(WAIT_FOR_PLAYER1_READ);
                return lastResult;
            case WAIT_FOR_PLAYER1_READ:
                return result;
            case WAIT_FOR_PLAYER1_ACTION:
                throw new WaitForPlayerException();
            default:
                throw new SetActionFirstException();
        }
    }

    public GameState getState() {
        return state;
    }

    public abstract Player getPlayer1();

    public abstract Player getPlayer2();

    public Ruleset getRuleset() {
        return ruleset;
    }

    public boolean setRuleset(Ruleset ruleset) {
        if (getState() == STARTED) {
            this.ruleset = ruleset;
            return true;
        } else
            return false;
    }

    @Override
    public String toString() {
        return "Game(player1 = " + getPlayer1() + ", player2 = " + getPlayer2() + ", player1Action = " + player1Action + ", player2Action = " + player2Action + ", status = " + state + ")";
    }
}
