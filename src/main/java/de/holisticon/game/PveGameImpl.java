package de.holisticon.game;

import de.holisticon.actions.Action;
import de.holisticon.exceptions.ActionAlreadySetException;
import de.holisticon.exceptions.ReadResultFirstException;
import de.holisticon.exceptions.UnsupportedActionException;
import de.holisticon.exceptions.WaitForReadException;
import de.holisticon.player.Computer;
import de.holisticon.player.Player;
import de.holisticon.player.User;
import de.holisticon.rulesets.Ruleset;

import java.util.ArrayList;
import java.util.List;

public class PveGameImpl extends AbstractGame implements PveGame {

    private User player;
    private Computer computer;
    private List<Action> computerActions;

    public PveGameImpl(User player, Ruleset ruleset) {
        super(ruleset);
        this.player = player;
        this.computer = new Computer();
        this.computerActions = new ArrayList<>(ruleset.getSupportedActions());
    }

    @Override
    public Player getPlayer1() {
        return player;
    }

    @Override
    public Player getPlayer2() {
        return computer;
    }

    @Override
    public boolean setRuleset(Ruleset ruleset) {
        boolean result = super.setRuleset(ruleset);
        if (result)
            computerActions = new ArrayList<>(ruleset.getSupportedActions());
        return result;
    }

    @Override
    public void setPlayer1Action(Action action) throws UnsupportedActionException, ActionAlreadySetException, WaitForReadException, ReadResultFirstException {
        super.setPlayer1Action(action);
        setPlayer2Action(computer.getAction(computerActions));
    }

    @Override
    public GameResult getPlayer1Result() {
        getPlayer2Result();
        return super.getPlayer1Result();
    }
}
