package de.holisticon.game;

import de.holisticon.player.User;
import de.holisticon.rulesets.Ruleset;

public class PvpGameImpl extends AbstractGame implements PvpGame {

    private User player1 = null;
    private User player2 = null;

    public PvpGameImpl(User player1, User player2, Ruleset ruleset) {
        super(ruleset);
        this.player1 = player1;
        this.player2 = player2;
    }

    @Override
    public User getPlayer1() {
        return player1;
    }

    @Override
    public User getPlayer2() {
        return player2;
    }
}
