package de.holisticon.game;

import de.holisticon.actions.Action;

/**
 * The different states a game {@link Game} can be in.
 */
public enum GameState {
    /**
     * Game just started and no action {@link Action} is performed yet.
     */
    STARTED,

    /**
     * Player2 {@link Game#getPlayer2()} has performed his action {@link Action} and waits for player1
     * {@link Game#getPlayer1()}.
     */
    WAIT_FOR_PLAYER1_ACTION,

    /**
     * Player1 {@link Game#getPlayer1()} has performed his action {@link Action} and waits for player2
     * {@link Game#getPlayer2()}.
     */
    WAIT_FOR_PLAYER2_ACTION,

    /**
     * The actions {@link Game#getPlayer1Action()} and {@link Game#getPlayer2Action()} were set and the
     * result is available.
     */
    RESULT_AVAILABLE,

    /**
     * Player2 {@link Game#getPlayer2()} has read the result {@link Game#getPlayer2Result()} and waits for player1
     * {@link Game#getPlayer1()} to read the result {@link Game#getPlayer1Result()}.
     */
    WAIT_FOR_PLAYER1_READ,

    /**
     * Player1 {@link Game#getPlayer1()} has read the result {@link Game#getPlayer1Result()} and waits for player2
     * {@link Game#getPlayer2()} to read the result {@link Game#getPlayer2Result()}.
     */
    WAIT_FOR_PLAYER2_READ,
}
