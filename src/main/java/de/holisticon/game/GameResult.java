package de.holisticon.game;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.holisticon.actions.Action;
import de.holisticon.actions.ActionResult;
import de.holisticon.player.Player;

/**
 * The result for a round of the game {@link Game}.
 */
@JsonSerialize(using = GameResultSerializer.class)
public class GameResult {
    Player player1;
    Player player2;
    Action player1Action;
    Action player2Action;
    ActionResult player1Result;
    ActionResult player2Result;

    public GameResult(Player player1, Player player2, Action player1Action, Action player2Action, ActionResult player1Result, ActionResult player2Result) {
        this.player1 = player1;
        this.player2 = player2;
        this.player1Action = player1Action;
        this.player2Action = player2Action;
        this.player1Result = player1Result;
        this.player2Result = player2Result;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Action getPlayer1Action() {
        return player1Action;
    }

    public void setPlayer1Action(Action player1Action) {
        this.player1Action = player1Action;
    }

    public Action getPlayer2Action() {
        return player2Action;
    }

    public void setPlayer2Action(Action player2Action) {
        this.player2Action = player2Action;
    }

    public ActionResult getPlayer1Result() {
        return player1Result;
    }

    public void setPlayer1Result(ActionResult player1Result) {
        this.player1Result = player1Result;
    }

    public ActionResult getPlayer2Result() {
        return player2Result;
    }

    public void setPlayer2Result(ActionResult player2Result) {
        this.player2Result = player2Result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameResult)) return false;

        GameResult that = (GameResult) o;

        if (!player1.equals(that.player1)) return false;
        if (!player2.equals(that.player2)) return false;
        if (player1Action != that.player1Action) return false;
        if (player2Action != that.player2Action) return false;
        if (player1Result != that.player1Result) return false;
        return player2Result == that.player2Result;

    }

    @Override
    public String toString() {
        return "GameResult{" +
                "player1=" + player1 +
                ", player2=" + player2 +
                ", player1Action=" + player1Action +
                ", player2Action=" + player2Action +
                ", player1Result=" + player1Result +
                ", player2Result=" + player2Result +
                '}';
    }
}
