package de.holisticon.game;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class GameResultSerializer extends JsonSerializer<GameResult> {
    @Override
    public void serialize(GameResult value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeFieldName("player1");
        jgen.writeStartObject();
        jgen.writeStringField("name", value.player1.getName());
        jgen.writeObjectField("action", value.player1Action);
        jgen.writeObjectField("result", value.player1Result);
        jgen.writeEndObject();
        jgen.writeFieldName("player2");
        jgen.writeStartObject();
        jgen.writeStringField("name", value.player2.getName());
        jgen.writeObjectField("action", value.player2Action);
        jgen.writeObjectField("result", value.player2Result);
        jgen.writeEndObject();
        jgen.writeEndObject();
    }
}
